#!/usr/bin/env python3

import json
import struct
import subprocess
import sys


def get_message():
    raw_length = sys.stdin.buffer.read(4)
    if len(raw_length) == 0:
        sys.exit(0)
    message_length = struct.unpack('@I', raw_length)[0]
    message = sys.stdin.buffer.read(message_length).decode('utf-8')
    return json.loads(message)


# Encode a message for transmission,
# given its content.
def encode_message(message_content):
    encoded_content = json.dumps(message_content).encode('utf-8')
    encoded_length = struct.pack('@I', len(encoded_content))
    return {'length': encoded_length, 'content': encoded_content}


# Send an encoded message to stdout
def send_message(encoded_message):
    sys.stdout.buffer.write(encoded_message['length'])
    sys.stdout.buffer.write(encoded_message['content'])
    sys.stdout.buffer.flush()


def execute():
    command_array = get_message()
    # Pipe subprocess output to stderr for it to be shown
    # in the firefox browser console
    result = subprocess.call(command_array, stdout=sys.stderr, stderr=sys.stderr, cwd="/tmp")
    send_message(encode_message("Exit status: %s" % result))


def install(copy=False):
    import json
    from pathlib import Path

    if not __file__:
        print("Script has to exist as a file! Cannot be piped to python", file=sys.stderr)

    this = Path(__file__)
    extension_namespace = "com.namingthingsishard.firefox.ccex"
    target_dir = Path("~/.mozilla/native-messaging-hosts").expanduser()
    target_manifest_path = target_dir / (extension_namespace + ".json")

    # Ensure manifest directory exists
    target_dir.mkdir(exist_ok=True)

    target_executor_path = this.absolute()
    # Copy executor
    if copy:
        target_executor_path = target_dir / (extension_namespace + ".py")
        target_executor_path.write_text(this.read_text())

    # Ensure executor can be run by firefox without any hocus-pocus
    target_executor_path.chmod(0o755)

    # Create and write manifest
    manifest = {
        "name": "com.namingthingsishard.firefox.ccex",
        "description": "Executes given commands",
        "path": str(target_executor_path),
        "type": "stdio",
        "allowed_extensions": ["ccex@firefox.namingthingsishard.com"]
    }
    target_manifest_path.write_text(json.dumps(manifest, indent=2))
    print("Wrote manifest to %s" % target_manifest_path)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Executes commands for the firefox extension",
        # exit_on_error=False,  # Execute firefox command by default
    )
    subparsers = parser.add_subparsers(
        dest='command',
    )
    parser_install = subparsers.add_parser(
        "install",
        description="Installs the executor and manifest to be executed by the extension",
    )
    parser_install.add_argument(
        "-c", "--copy",
        help="Copy the executor beside the manifest. Use when executing script from temporary path",
        action="store_true"
    )

    if len(sys.argv) > 1 and sys.argv[1] not in subparsers.choices:
        execute()
    else:
        args = parser.parse_args()
        if args.command == "install":
            install(copy=args.copy)
        else:
            parser.print_usage()
