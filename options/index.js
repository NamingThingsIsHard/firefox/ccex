import {buildMenus} from "../util.js";

let CommandFormUi = {
    generate(id = null) {
        let $template = document.querySelector('#command-form-template');
        let $form = document.importNode($template.content, true).firstElementChild;
        this.bindFormEvents($form);
        if (id) {
            $form.id = String(id);
        }
        return $form
    },

    $getLabelInput($form) {
        return $form.querySelector(".label-input");
    },

    $getCommandTextArea($form) {
        return $form.querySelector(".command-input");
    },

    $getSaveButton($form) {
        return $form.querySelector(".save-button");
    },

    $getRemoveButton($form) {
        return $form.querySelector(".remove-button");
    },

    bindFormEvents($form) {
        let $saveButton = this.$getSaveButton($form);
        $saveButton.addEventListener("click", () => CommandFormAction.save($form));

        let $removeButton = this.$getRemoveButton($form);
        $removeButton.addEventListener("click", () => CommandFormAction.remove($form))
    },

    fillCommandForm({label = "", commandArray = [], $form = null, id = null}) {
        $form = $form || this.generate(id);
        let $labelInput = this.$getLabelInput($form);
        $labelInput.value = label;
        let $commandTextArea = this.$getCommandTextArea($form);
        $commandTextArea.value = commandArray.join("\n");
        return $form
    }
};

let CommandFormAction = {
    save($form) {
        const commandId = $form.id;
        let $labelInput = CommandFormUi.$getLabelInput($form);
        let $commandTextArea = CommandFormUi.$getCommandTextArea($form);
        let commandObject = {
            label: $labelInput.value,
            commandArray: $commandTextArea.value
                .split("\n")
                .map((line) => line.trim())
        };
        browser.storage.sync.set({
            [commandId]: commandObject
        }).then(buildMenus)
            .then(() => {
                console.info("saved command", commandId);
            });
    },

    remove($form) {
        let commandId = $form.id;
        browser.storage.sync.remove(commandId)
            .then(() => browser.contextMenus.remove(commandId))
            .then(() => console.info(`removed command ${commandId}`))
            .finally(() => {
                $form.parentNode.removeChild($form);
            })
    }
};

let $forms = document.querySelector(".forms");
document.querySelector(".add-form").addEventListener("click", () => {
    $forms.appendChild(CommandFormUi.generate(Date.now()));
})

browser.storage.sync.get().then((_storage) => {
    Object.keys(_storage).forEach((commandId) => {
        let commandObject = _storage[commandId];
        let args = Object.assign({
            id: commandId
        }, commandObject);
        $forms.appendChild(CommandFormUi.fillCommandForm(args));
    })
});
