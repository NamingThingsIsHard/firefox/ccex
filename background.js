import {buildMenus, CONTEXT_ATTRS} from "./util.js";

/**
 * Send the command to the executor app
 */
browser.contextMenus.onClicked.addListener(async (info, tab) => {
    const [commandId, type] = info.menuItemId.split('-')
    let url;
    // Media and page
    if (type) {
        url = info[CONTEXT_ATTRS[type]]
    } else {
        // Bookmark, link, tab
        if (info.bookmarkId) {
            const bookmarks = await browser.bookmarks.get(info.bookmarkId)
            if (bookmarks.length > 0) {
                url = bookmarks[0].url
            }
        } else {
            url = info.linkUrl ? info.linkUrl : info.pageUrl
        }
    }
    if (!url) {
        return
    }
    browser.storage.sync.get(commandId).then((commandObject) => {
        commandObject = commandObject[commandId];
        const message = commandObject.commandArray.map((line) => line.replace("$URL$", url));

        // Send command to executor
        // Described in https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging
        browser.runtime.sendNativeMessage(
            "com.namingthingsishard.firefox.ccex", message
        ).then((response) => {
            console.info(`Response of ${message}`, response)
        }, (errorResponse) => {
            console.error(`Response of ${message}`, errorResponse)
        })
    })
})

// Build the menu of commands
buildMenus();
