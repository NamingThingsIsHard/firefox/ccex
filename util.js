export const CONTEXT_ATTRS = {
  "audio": "srcUrl",
  "image": "srcUrl",
  "page": "pageUrl",
  "video": "srcUrl",
};
const CONTEXTS = Object.keys(CONTEXT_ATTRS);


export async function buildMenus() {
  await browser.contextMenus.removeAll()
  await createMenuParents();
  const _storage = await browser.storage.sync.get();
  for (let commandId of Object.keys(_storage)) {
    let commandObject = _storage[commandId];
    await buildMenu(commandId, commandObject);
  }
}

async function createMenuParents() {
  // Create parents
  for (let context of CONTEXTS) {
    await browser.contextMenus.create({
      id: context,
      title: context[0].toUpperCase() + context.substr(1),
      contexts: [context]
    })
  }
  console.debug(`Created parents`)
}


async function buildMenu(commandId, commandObject) {
  const label = commandObject.label;
  const logPrefix = `create:"${label}:`;

  // These are handled implicitly
  await browser.contextMenus.create({
    id: `${commandId}`,
    title: label || commandId,
    contexts: [
      "bookmark",
      "link",
      "tab",
    ]
  });
  console.debug(`${logPrefix} Created main`)

  // All other contexts have specific handling
  for (let context of CONTEXTS) {
    await browser.contextMenus.create({
      id: `${commandId}-${context}`,
      title: commandObject.label || commandId,
      parentId: context,
      contexts: [context]
    });
    console.debug(`${logPrefix} Created "${context}"`)
  }

}
