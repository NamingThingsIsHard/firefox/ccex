# Context Command EXecutor

A simple firefox extension that allows configuring and executing commands for right-clicks on links,
 pages, bookmarks and tabs.

**Examples**

__gimp__

```
gimp
$URL$
```

Open an image directly in gimp

__mpv__

```
mpv
--force-window=yes
--cache=yes
--demuxer-max-bytes=512MiB
$URL$
```

Open a youtube, peertube, soundcloud webpage 
 (or any of the domains in the [extensive list supported][ytdl-extractors] by youtube-dl) in mpv.


[ytdl-extractors]: https://github.com/ytdl-org/youtube-dl/tree/master/youtube_dl/extractor

# Development

**Requirements**

 - [npm]
 - [python 3]

**Getting started**

```bash
# Install JS dependencies
npm install

# Start a dev session
npm start
```

# How this works

First we have to register context menu handlers (right-click on a part of the browser).
Depending on the type of item we right-clicked on (webpage, image, video, etc.) we might fill the context menu.

Once a menu item is clicked, the corresponding command is executed asynchronously:
 we don't wait for a response otherwise that might lock up the UI.

You can have a look in the browser console (Ctrl+Shift+J) to see the results of the execution.

## Executing commands

The extension uses "native messaging" which is a fancy way of saying the browser calls a trusted script
 and passes values to it.
In the case of this extension, the script in question is [executor.py].

It's a simple python script that reads the values passed from firefox.
Those values contain an array which is passed as is to [`subprocess.call`][subprocess.call].

**Security**

You might say "that's super dangerous, it can execute anything!"... well, maybe.
The script has to be registered with firefox using a manifest (look in [executor.py]).
That manifest tells firefox:

 - which script may be executed (in our case `executor.py`)
 - which extension may execute the script (only this one)
 - other metadata

# Further reading

 - [Python stdlib doc][pydoc]
 - [Webextensions documentation][webextensions]
 - Webextension [native messaging]

---------------------

[executor.py]: ./native/executor.py
[native messaging]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging
[npm]: https://www.npmjs.com/
[python 3]: https://www.python.org/downloads/
[pydoc]: https://docs.python.org/3/
[subprocess.call]: https://docs.python.org/3/library/subprocess.html?highlight=subprocess%20call#subprocess.call
[webextensions]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions
